import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt


def plot_nodes(data,label=''):

    mpl.rcParams['legend.fontsize'] = 10
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(data[:,0], data[:,1], data[:,2],'o', label=label)

    ax.legend()

if __name__ == '__main__':
    data = np.random.random((10,3))

    plot_nodes(data,'10 random points')
    plt.show()