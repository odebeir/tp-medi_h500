"""
csvfileio.py

this module groups methods responsible for reading .csv file
- only frames with 40 markers are kept
- both XYZ and TimeStamp data are returned
"""


import numpy as npy

def readcsv(filename):
    """import csv file, reject all non 40 point record, this can be enhanced in the future
    returns list of (timestamp,xyz array)"""
    data = []
    for line in open(filename):
        s = line.split(',')
        if s[0] == 'frame':
            n_points = int(s[4])
            if n_points==40:
                try:
                    timestamp = float(s[2])
                    xyz = npy.reshape(npy.array(s[5:5+n_points*3]).astype(float),(n_points,3))
                    data.append((timestamp,xyz))
                except ValueError :
                    print 'warning error during conversion'
    return data

if __name__ == '__main__':
    print 'test csb'
    data = readcsv('../data/hand_piano_referencel.csv')
    print 'data available: ',len(data)
    print 'example:'
    print data[10]
