from csvfileio import readcsv
from plot3D import plot_nodes
import matplotlib.pyplot as plt


def main():
    data = readcsv('../data/hand_piano_referencel.csv')
    timestamp,nodes = data[10]
    plot_nodes(nodes,'ref%f'%timestamp)
    plt.show()


if __name__ == '__main__':
    main()